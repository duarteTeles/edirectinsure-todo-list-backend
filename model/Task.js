const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');

const TaskSchema = mongoose.Schema({
  state: {
    type: String,
    enum: ['todo', 'done'],
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  creationDate: {
    type: Date,
    default: Date.now(),
  },
  finishDate: {
    type: Date,
    required: true,
  },
  userId: {
    type: Number,
    required: true,
  },
  projectId: {
    type: Number,
    required: true,
  },
  taskId: {
    type: Number,
  },
});

autoIncrement.initialize(mongoose.connection);
TaskSchema.plugin(autoIncrement.plugin, {
  model: 'TaskSchema',
  field: 'task_id',
  startAt: 1,
  incrementBy: 1,
});

// export model task with TaskSchema
module.exports = mongoose.model('task', TaskSchema);
