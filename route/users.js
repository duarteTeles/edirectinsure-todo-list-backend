const express = require('express');

const expressValidator = require('express-validator');

const {check, validationResult} = expressValidator;
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const {auth, getAuthenticatedUser} = require('../middleware/authentication');

const usersRouter = express.Router();

const User = require('../model/User');
const {JWT_SIGN_SECRET, JWT_SIGN_EXPIRES_IN} = require('../config/jwt.config');


/* GET users listing. */
usersRouter.get('/', (req, res, next) => {
  res.send('respond with a resource');
});

usersRouter.post('/login',
    [
      check('email', 'Please enter a valid email').isEmail(),
      check('password', 'Please enter a valid password').isLength({
        min: 6, max: 20,
      }),
    ],
    async (req, res, next) => {
      const errors = validationResult(req);

      if (!errors.isEmpty()) {
        return res.status(400).json({
          errors: errors.array(),
        });
      }

      const {email, password} = req.body;
      try {
        const user = await User.findOne({
          email,
        });
        if (!user) {
          return res.status(400).json({
            message: 'User does not exist',
          });
        }

        const isMatch = await bcrypt.compare(password, user.password);
        if (!isMatch) {
          return res.status(400).json({
            message: 'Invalid password',
          });
        }

        const payload = {
          user: {
            id: user.id,
          },
        };

        jwt.sign(
            payload,
            JWT_SIGN_SECRET,
            {
              expiresIn: JWT_SIGN_EXPIRES_IN,
            },
            (err, token) => {
              if (err) throw err;
              res.status(200).json({
                token,
              });
            },
        );
      } catch (e) {
        console.error(e);
        res.status(500).json({
          message: 'Server Error',
        });
      }
    });


usersRouter.post('/register', [
  check('email', 'Please insert a valid email address')
      .not()
      .isEmpty().isEmail(),
  check('password', 'Please insert a valid password').isLength({
    min: 6, max: 20,
  }),
  check('firstName', 'Please insert your first name ')
      .not()
      .isEmpty(),
  check('lastName', 'Please insert your last name')
      .not()
      .isEmpty(),
], async (req, res, next) => {
  // res.status(501).json({ message: 'Not Implemented' });
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }

  const {
    email, password, firstName, lastName,
  } = req.body;


  try {
    let user = await User.findOne({
      email,
    });
    if (user) {
      return res.status(400).json({
        message: 'User already exists',
      });
    }


    user = new User({
      email,
      password,
      firstName,
      lastName,
    });

    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(password, salt);
    await user.save();

    const payload = {
      user: {
        id: user.id,
      },
    };


    jwt.sign(
        payload,
        JWT_SIGN_SECRET, {
          expiresIn: JWT_SIGN_EXPIRES_IN,
        },
        (err, token) => {
          if (err) throw err;
          res.status(200).json({
            token,
          });
        },
    );
  } catch (err) {
    console.log(err.message);
    res.status(500).send('Error in saving token');
  }
});


usersRouter.get('/details', auth, getAuthenticatedUser, ((req, res, next) => {
  res.status(200).json(req.authenticatedUser);
}));


module.exports = usersRouter;
