const bodyParser = require('body-parser');
const cors = require('cors');
const path = require('path');
const errorHandler = require('errorhandler');
const morgan = require('morgan');
const express = require('express');


const app = express();
const PORT = process.env.PORT || 3000;
const InitializeMongoDB = require('./config/database.config');


const apiRouter = require('./route/api');

app.use(cors());
app.use(errorHandler());
app.use(morgan('dev'));

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended: false}));
app.use(express.static(path.join(__dirname, 'public')));

// parse application/json
app.use(bodyParser.json());

app.listen(PORT, () => {
  console.log(`Server listening on http://localhost:${PORT}`);
});

// Start MongoDB
InitializeMongoDB().then(() => {

});


app.use('/api', apiRouter);


module.exports = app;
