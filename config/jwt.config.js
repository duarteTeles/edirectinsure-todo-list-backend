const JWT_SIGN_SECRET = 'edirectTodoSecret';
const JWT_SIGN_EXPIRES_IN = 300000;

module.exports = {JWT_SIGN_SECRET, JWT_SIGN_EXPIRES_IN};
