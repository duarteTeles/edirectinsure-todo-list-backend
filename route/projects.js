const express = require('express');
const expressValidator = require('express-validator');

const {check, validationResult} = expressValidator;

const projectsRouter = express.Router({mergeParams: true});
const {auth, getAuthenticatedUser, validateUser} = require('../middleware/authentication');
const Project = require('../model/Project');


projectsRouter.param('projectId', async (req, res, next, projectId) => {
  try {
    const userProject = await Project.findOne({project_id: projectId});
    if (!userProject) {
      res.status(404).json({message: `Project with ID ${projectId} not found`});
    } else {
      req.authenticatedUserProject = userProject;
      next();
    }
  } catch (error) {
    res.status(500).json({
      message: error.message || 'Some error occurred while retrieving user projects.',
    });
  }
});


projectsRouter.get('/', auth, getAuthenticatedUser, validateUser, async (req, res, next) => {
  const {userId} = req.params;

  try {
    const userProjects = await Project.find({userId});
    if (userProjects.length === 0) {
      res.status(200).json({message: 'There are no projects!'});
    } else {
      res.status(200).json(userProjects);
    }
  } catch (error) {
    res.status(500).send({
      message: error.message || 'Some error occurred while retrieving user projects.',
    });
  }
});

projectsRouter.post('/', auth, getAuthenticatedUser, validateUser, [
  check('name', 'Please enter a valid project name').isLength({
    min: 4,
  }),
  check('tasks', 'When creating a new project, by default the tasks array must be empty').isArray().isEmpty(),
], async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }

  const {name, tasks} = req.body;
  const {userId} = req.params;


  // Create a new Project
  const project = new Project({
    name,
    tasks,
    userId,
  });

  try {
    await project.save();
    res.sendStatus(201);
  } catch (error) {
    res.status(500).json({message: error.message || 'Some error occurred while saving the project.'});
  }
});

projectsRouter.put('/:projectId', auth, getAuthenticatedUser, validateUser, [
  check('name', 'Please enter a valid project name').isLength({
    min: 4, max: 20,
  }),
], async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }


  const {projectId} = req.params;

  const set = {};
  set.name = req.body.name;

  if (Object.keys(set).length === 0) {
    return res.status(400).json({
      message: 'Cannot update empty fields',
    });
  }

  try {
    const project = await Project.findOneAndUpdate({
      project_id: projectId,
    }, {

      $set:
      set,
    }, {
      new: true,

    });
    res.send(project);
  } catch (error) {
    return res.status(500).send({
      message: `Error updating project with project ID ${projectId}`,
    });
  }
});

projectsRouter.delete('/:projectId', async (req, res, next) => {
  const {projectId} = req.params;
  try {
    await Project.findOneAndDelete({project_id: projectId});
    res.sendStatus(204);
  } catch (error) {
    res.status(500).json({error: `Error while deleting project with ID ${projectId}`});
  }
});


module.exports = projectsRouter;
