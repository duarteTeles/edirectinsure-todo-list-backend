const jwt = require('jsonwebtoken');
const {JWT_SIGN_SECRET} = require('../config/jwt.config');
const User = require('../model/User');

const auth = (req, res, next) => {
  const token = req.header('token');
  if (!token) return res.status(401).json({message: 'Unauthorized'});

  try {
    const decoded = jwt.verify(token, JWT_SIGN_SECRET);
    req.user = decoded.user;
    next();
  } catch (e) {
    console.error(e);
    res.status(500).send({message: 'Invalid token'});
  }
};


const validateUser = (req, res, next) => {
  const userIdParam = parseInt(req.params.userId, 10);
  const userIdReq = parseInt(req.authenticatedUser.userId, 10);

  if (userIdParam !== userIdReq) {
    res.status(400).json({error: 'Authenticated User ID does not match User ID passed in parameter'});
  } else {
    next();
  }
};


const getAuthenticatedUser = async (req, res, next) => {
  try {
    // request.user is getting fetched from Middleware after token authentication
    const user = await User.findById(req.user.id);
    const {
      firstName, lastName, email,
    } = user;
    const userId = user.user_id;
    req.authenticatedUser = {
      email, firstName, lastName, userId,
    };
    next();
  } catch (e) {
    res.status(500).json({message: 'Error in getting user'});
  }
};

module.exports = {auth, getAuthenticatedUser, validateUser};
