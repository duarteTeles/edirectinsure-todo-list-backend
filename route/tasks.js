const express = require('express');
const expressValidator = require('express-validator');
const moment = require('moment');

moment().format();
const MOMENT_DATE_FORMAT = 'MM-DD-YYYY';
const STATE_ENUM = ['todo', 'done'];


const {check, validationResult, body} = expressValidator;


const tasksRouter = express.Router({mergeParams: true});
const {auth, getAuthenticatedUser, validateUser} = require('../middleware/authentication');
const Task = require('../model/Task');
const Project = require('../model/Project');


tasksRouter.param('taskId', async (req, res, next, taskId) => {
  const {projectId} = req.params;
  try {
    const userProjectTask = await Task.findOne({task_id: taskId});
    if (!userProjectTask) {
      res.status(404).json({message: `Task with ID ${taskId} not found for project with ${projectId}`});
    } else {
      req.userProjectTask = userProjectTask;
      next();
    }
  } catch (error) {
    res.status(500).json({
      message: error.message || `Some error occurred while retrieving tasks for project ${projectId}`,
    });
  }
});

tasksRouter.get('/', auth, getAuthenticatedUser, validateUser, async (req, res, next) => {
  const {projectId, userId} = req.params;

  try {
    const userProjectTasks = await Task.find({userId, projectId});
    if (userProjectTasks.length === 0) {
      res.status(200).json({message: `There are no tasks for project ${projectId}!`});
    } else {
      res.status(200).json(userProjectTasks);
    }
  } catch (error) {
    res.status(500).send({
      message: error.message || `Some error occurred while retrieving tasks for project ${projectId}.`,
    });
  }
});

tasksRouter.post('/', auth, getAuthenticatedUser, validateUser, [
  check('description', 'Please enter a valid task description').notEmpty(),
], body('state').custom((value) => {
  let validState = false;
  STATE_ENUM.forEach((state) => {
    if (state === value) {
      validState = true;
    }
  });
  if (validState) {
    return true;
  }
  throw new Error('The task can only be with state done or todo');
}), body('finishDate').custom((value) => {
  const isDateValid = moment(value, MOMENT_DATE_FORMAT).isValid();
  if (!isDateValid) {
    throw new Error(`The finish date is not a valid date ${MOMENT_DATE_FORMAT}`);
  } else {
    return true;
  }
}), async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array(),
    });
  }
  const {description, state, finishDate} = req.body;

  const {userId, projectId} = req.params;

  const task_temp = await Task.findOne({projectId});
  console.log(task_temp);
  const taskId = 1;


  // create a new Task
  const task = new Task({
    state,
    description,
    finishDate,
    userId,
    projectId,
  });


  try {
    await task.save();
    const task_temp = await Task.findOne({projectId});
    const taskId = task_temp.task_id;
    task.taskId = taskId;

    try {
      await Project.findOneAndUpdate(
          {project_id: projectId},
          {
            $push: {
              tasks: task,
            },
          },
      );
    } catch (error) {
      return res.status(500).send({
        message: `Error saving task for project with ID ${projectId}`,
      });
    }
    res.sendStatus(201);
  } catch (error) {
    res.status(500).json({message: error.message || `Some error occurred while saving the task for project ${projectId}.`});
  }
});

tasksRouter.put('/:taskId', auth, getAuthenticatedUser, validateUser, [
  check('description', 'Please enter a valid task description').notEmpty(),
], body('state').custom((value) => {
      let validState = false;
      STATE_ENUM.forEach((state) => {
        if (state === value) {
          validState = true;
        }
      });
      if (validState) {
        return true;
      }
      throw new Error('The task can only be with state done or todo');
    }),
// Note: removed finishDate validation because it was causing problems in the frontend. To be fixed later on.
//     body('finishDate').custom((value) => {
//   const isDateValid = moment(value, MOMENT_DATE_FORMAT).isValid();
//   if (!isDateValid) {
//     throw new Error(`The finish date is not a valid date ${MOMENT_DATE_FORMAT}`);
//   } else {
//     return true;
//   }
// }),
    async (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {

        return res.status(400).json({
          errors: errors.array(),
        });
      }


      const {description, state, finishDate} = req.body;

      const {projectId, taskId} = req.params;

      const set = {
        description,
        finishDate,
        projectId,
      };


  if (state === 'done') {
    set.state = state;
  }


  if (Object.keys(set).length === 0) {
    return res.status(400).json({
      message: 'Cannot update empty fields',
    });
  }

  try {
    const task = await Task.findOneAndUpdate({
      task_id: taskId,
    }, {
      $set:
      set,
    }, {
      new: true,
    });
    res.status(201).json(task);
  } catch (error) {
    return res.status(500).send({
      message: `Error updating task with project ID ${projectId} and task ID ${taskId}`,
    });
  }
});

tasksRouter.delete('/:taskId', async (req, res, next) => {
  const {taskId, projectId} = req.params;
  try {
    const task = await Task.findOne({task_id: taskId});

    // state todo
    if (task.state === STATE_ENUM[0]) {
      await task.deleteOne({task_id: taskId});
      res.sendStatus(204);
    } else {
      res.status(400).json({error: 'Cannot delete tasks with state = done'});
    }
  } catch (error) {
    res.status(500).json({error: `Error while deleting project with ID ${projectId} and task with ${taskId}`});
  }
});


module.exports = tasksRouter;
