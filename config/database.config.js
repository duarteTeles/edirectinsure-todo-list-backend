const mongoose = require('mongoose');

const MONGO_DATABASE_NAME = 'EdirectInsureTodoDatabase';
const MONGO_DATABASE_PORT = '27017';
const SERVER_URL = 'localhost';
const MONGO_URI = `mongodb://${SERVER_URL}:${MONGO_DATABASE_PORT}/${MONGO_DATABASE_NAME}`;

mongoose.set('useCreateIndex', true);

const InitializeMongoDB = async () => {
  try {
    await mongoose.connect(MONGO_URI, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
    });
    console.log('Successfully connected to the database');
  } catch (e) {
    console.log('Could not connect to the database. Exiting now...', e);
    process.exit();
  }
};

module.exports = InitializeMongoDB;
