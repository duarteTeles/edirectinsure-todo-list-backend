const express = require('express');
const apiRouter = express.Router();

const projectsRouter = require('./projects');
const usersRouter = require('./users');
const tasksRouter = require('./tasks');

apiRouter.use('/users', usersRouter);
usersRouter.use('/:userId/projects', projectsRouter);
projectsRouter.use('/:projectId/tasks', tasksRouter);


module.exports = apiRouter;
