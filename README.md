# EdirectInsure TODO App Interview Exercise Solutions

This is the main README for the Edirect TODO app - backend. It contains all the necessary instructions for anyone to setup the backend of the Todo App.

## Getting Started

THe following sections describe how anyone can get this project setup and running.

### Prerequisites

The following is required to be installed on your machine:

1. Node version 12.13.0 or greater
2. npm version 6.12.0 or greater
3. MongoDB version 4.2.6  (by default the database is configured on Port 27017 with name EdirectInsureTodoDatabase)


### Setup and Installation

To setup up this project, first clone the project:

```
git clone https://duarteTeles@bitbucket.org/duarteTeles/edirectinsure-todo-list-backend.git
```

Then navigate to the project folder and install all the required dependencies:

```
cd edirectinsure-todo-list-backend && npm install
```


Start NodeJS server (by default it runs on PORT 3000):

```
npm start
```

or

```
node server.js
```


## Folder Structure

**config** : contains the database and JWT (JSON Web Token) configurations  
**middleware**: contains custom-made middleware for authentication with JWT
**model**: contains all the mongoose models   
**public**: contains all the public files   
**route**: contains all the routes used in this server 
**test**: should contain all the unit tests, but, due to lack of time, they were not implemented

# Routes
## Users
* POST /api/users/login
* POST /api/users/register
* GET /api/users/details
## Projects
*  GET /api/users/:userId/projects
* POST /api/users/:userId/projects
* PUT /api/users/:userId/projects/:projectId
* DELETE /api/users/:userId/projects/:projectId
## Tasks
* GET /api/users/:userId/projects/:projectId/tasks
* POST /api/users/:userId/projects/:projectId/tasks
* PUT /api/users/:userId/projects/:projectId/tasks/:taskId
* DELETE /api/users/:userId/projects/:projectId/tasks/:taskId

## Built With

* [NodeJS](https://nodejs.org/en/) - A JavaScript runtime built on Chrome's V8 JavaScript engine
* [ExpressJS](https://expressjs.com/) - Fast, unopinionated, minimalist web framework for Node.js
* [MongoDB](https://www.mongodb.com/) - A general purpose, document-based, distributed database built for modern application developers and for the cloud era.
* [Mongoose](https://mongoosejs.com/) - Elegant mongodb object modeling for node.js

## Authors

* **Duarte Teles** - [Bitbucket](https://bitbucket.org/%7B65e86de9-c623-412d-8584-596b396bbb67%7D/)

# Improvements/ future work
1. Have a Docker container running the backend of the TODO App
2. Use Swagger os similar to document this nodeJS API with HTTP Status codes and possible responses and errors
3. Run and develop unit tests with [Request](https://github.com/request/request)


## Acknowledgments

I would like to thank Edirect Insure for this amazing opportunity to show my value. I have had a lot of fun developing this project.

