const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');

const ProjectSchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    tasks: {
        type: Array,
        required: true,
    },
    userId: {
        type: Number,
        required: true,
    },
});

autoIncrement.initialize(mongoose.connection);
ProjectSchema.plugin(autoIncrement.plugin, {
    model: 'ProjectSchema',
    field: 'project_id',
    startAt: 1,
    incrementBy: 1,
});

// export model project with ProjectSchema
module.exports = mongoose.model('project', ProjectSchema);
