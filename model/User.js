const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');

const UserSchema = mongoose.Schema({
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  createdAt: {
    type: Date,
    default: Date.now(),
  },
  firstName: {
    type: String,
    required: true,
  },
  lastName: {
    type: String,
    required: true,
  },
});

autoIncrement.initialize(mongoose.connection);
UserSchema.plugin(autoIncrement.plugin, {
  model: 'UserSchema',
  field: 'user_id',
  startAt: 1,
  incrementBy: 1,
});

// export model user with UserSchema
module.exports = mongoose.model('user', UserSchema);
